# Change Log

## [Future Release](https://gitlab.com/devvyn/odd-ocelot/tree/master)

### Add minimal project files

- Source control:
    - [`.gitignore`](https://gitlab.com/devvyn/odd-ocelot/blob/master/.gitignore)
- Documentation:
    - [`CHANGELOG.md`](https://gitlab.com/devvyn/odd-ocelot/blob/master/CHANGELOG.md)
    - [`LICENSE.md`](https://gitlab.com/devvyn/odd-ocelot/blob/master/LICENSE.md)
    - [`README.md`](https://gitlab.com/devvyn/odd-ocelot/blob/master/README.md)

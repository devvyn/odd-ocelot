# Project code name: Odd Ocelot

## Concept

### Brief

A self aware software platform for modular intelligence and extensible agency, for assisted fulfillment of human goals.
At some ultimate level, humans are to be treated as drivers, but at the same time humans will be left out of operations except as necessary for optimal fulfillment of those human-driven intentions.
At a grand scale, Odd Ocelot is a platform framework for evolving something truly awesome.

### Basis

Given these assumptions:

- all problems of interest to humans can be solved by knowing and doing,
- anything that is possible to do can be described in some combination of natural language, programming language, and parcelled data.
- it is always possible to determine whether a given piece of software has the means to solve the problem

The platform in tandem with installed solution modules uses an understanding of what, how, when, where _and why_ it should do the right thing,
so that it can always be maximally helpful to the relevant humans in any particular case.

Deployed platform instances automatically operate as a decentralized network, scaling and extending as necessary.
Human interaction, when involved, happens by whichever mode and through whichever node is appropriate, depending on the nature of the interaction.

All of problem solving necessities can be packaged together as an installable solution module for the Odd Ocelot platform.

## Operational Principals

* Prompt appropriate humans for help only when needed.
* Part of what drives this project is my desire to explore the feasibility of developing an initially featureless application into whatever it needs to be for general utility.
* At all stages of release, the entire system will separate platform features from all user utilities, and adhere to the tenets of the [twelve-factor app methodology](https://12factor.net/).
* Odd Ocelot uses what it knows to solve any problem.
* Odd Ocelot knows about its own capabilities and the functionality of resources in its extended operational environment.
* Whenever external resources require an active agent, and that active agent can be Odd Ocelot, Odd Ocelot will deploy itself to that external computer system with all the necessary modules and data. Through self replication, Odd Ocelot builds a decentralized network of functionality.
* Optimal acquisition, availability, and interpretation of information is key to achieving optimal results wherever some piece of that information is important. Therefore, nodes in the network share information so they can adapt quickly, acting in concert.
* Humans are considered resources, too. Odd Ocelot uses its resources to communicate appropriately with humans in order to succeed in an effort whenever some piece of a solution is missing.

## Platform Features

The only capabilities built into the platform itself are:

* At least one mode of machine-machine interaction that can pass information in both directions.
* The ability to gain additional capabilities through installable modules.

## Solution Modules

Every solution module contributes at least one of the following:

- Human readable documentation
- Clear, expressive source code
- Human readable data

Arguably, all three of these are actually one and the same, in an ideal world.
That's the world Odd Ocelot comes from.

## Road Map

### Starting point

A valid software project with no features.

### Next features

- Odd Ocelot will gain the capability to communicate with human users through a web interface.
- How To instructions for using Odd Ocelot.